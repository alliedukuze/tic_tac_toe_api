require 'sinatra'

#Endpoint
get '/' do
  board = params['board'].downcase

  halt 400 unless !board.empty?

  # validating the board is the right size and there's no characters other than x,o, and spaces
  halt 400 unless board_valid board

  # initializing board
  board_initialization board

  halt 400 unless entries_valid

  # Checking if there's a winner before executing the next_move
  check_board
end

def board_valid(board)
  ((board.length == 9) || (board.empty?)) && (board.delete('xo ').empty?)
end


def board_initialization(board)
  @r1a, @r1b, @r1c, @r2a, @r2b, @r2c, @r3a, @r3b, @r3c = board.split('')
end

def entries_valid
  if (tiles - [' ']).empty?
    true
  else
    x_count = tiles.count('x')
    o_count = tiles.count('o')
    diff = (x_count - o_count).abs
    ((o_count <= x_count) && (diff <= 1))
  end
end

def check_board
  winning_comb.each{|c|
    if (c - ['o']).empty?
      return tiles
    elsif (c - ['x']).empty?
      return halt 400
    end
  }
  next_move
  tiles.join('')
end

def next_move
  tiles.shuffle.each {|t|
    if t == ' '
      t.gsub!(' ', 'o')
      return
    end
  }
end

def tiles
  [
    @r1a, @r1b, @r1c,
    @r2a, @r2b, @r2c,
    @r3a, @r3b, @r3c
  ]
end

def winning_comb
  [
    [@r1a, @r1b, @r1c],
    [@r2a, @r2b, @r2c],
    [@r3a, @r3b, @r3c],
    [@r1a, @r2a, @r3a],
    [@r1b, @r2b, @r3b],
    [@r1c, @r2c, @r3c],
    [@r1a, @r2b, @r3c],
    [@r1c, @r2b, @r3a]
  ]
end
